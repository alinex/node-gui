import routes from './extend'

// core routes
routes.push({
  path: '/',
  component: () => import('layouts/default.vue'),
  children: [
    { path: '', redirect: '/start' },
    { path: 'start', component: () => import('pages/index') },
    { path: 'login', component: () => import('pages/index') },
    {
      path: 'info',
      component: () => import('pages/info'),
      meta: { module: 'core.info' }
    },

    {
      path: 'users',
      component: () => import('pages/users'),
      meta: { module: 'core.users', ability: ['read', 'users'] }
    },
    {
      path: 'users/:id',
      component: () => import('pages/users/details'),
      meta: { module: 'core.users', ability: ['read', 'users'] }
    },

    {
      path: 'roles',
      component: () => import('pages/roles'),
      meta: { module: 'core.roles', ability: ['read', 'roles'] }
    },
    {
      path: 'roles/:id',
      component: () => import('pages/roles/details'),
      meta: { module: 'core.roles', ability: ['read', 'roles'] }
    }
  ]
})

// Always leave this as last one
routes.push({
  path: '*',
  component: () => import('pages/error404.vue')
})

export default routes
