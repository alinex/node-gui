import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

Vue.use(VueI18n)

const dateTimeFormats = {
  'en-US': {
    short: {
      year: 'numeric', month: 'short', day: 'numeric'
    },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
      hour: 'numeric',
      minute: 'numeric'
    }
  },
  de: {
    short: { day: 'numeric', month: 'short', year: 'numeric' },
    long: {
      weekday: 'short',
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    }
  }
}

const numberFormats = {
  'en-US': {
    currency: {
      style: 'currency', currency: 'USD'
    },
    percent: {
      style: 'percent'
    }
  },
  de: {
    currency: {
      style: 'currency', currency: 'EUR', currencyDisplay: 'symbol'
    },
    percent: {
      style: 'percent'
    }
  }
}

const i18n = new VueI18n({
  locale: 'de',
  fallbackLocale: 'en-us',
  messages,
  dateTimeFormats,
  numberFormats
})

export default ({ app }) => {
  // Set i18n instance on app
  app.i18n = i18n
}

export { i18n }
