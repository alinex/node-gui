/* eslint-disable quotes */
import { abilitiesPlugin } from '@casl/vue'
import { Ability } from "@casl/ability"
import { Notify } from 'quasar'

export default ({ Vue, router, store, ability }) => {
  Vue.use(abilitiesPlugin, new Ability([]))
  // can be updated with this.$ability.update(...)

  router.beforeEach((to, from, next) => {
    let redirect = false

    const ability = new Ability(store.state.auth.user?.abilities || [])
    // check for auth
    if (to.matched.some(record => record.meta.ability)) {
      if (!store.state.auth.user) {
        console.log('Redirected to login because authentication is needed!')
        next({ path: '/login' })
      } else if (!ability.can(...to.meta.ability)) {
        redirect = `You are not allowed to ${to.meta.ability.join(' ')}!`
      }
    } else next()

    // define specific abilities per page
    // if (to.matched.some(record => record.path === '/test') && !ability.can('read', 'users')) { redirect = 'You are not allowed to read user objects.' }

    if (redirect) {
      console.warn(`Redirected to root because of: ${redirect}`)
      Notify.create({
        type: 'negative',
        message: router.app.$t('layout.failed'),
        caption: redirect
      })
      next({ path: '/' })
    } else next()
  })
}
