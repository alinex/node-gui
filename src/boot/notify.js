import { Notify } from 'quasar'

Notify.setDefaults({
  timeout: 5000,
  progress: true,
  actions: [{ icon: 'close', color: 'white' }]
})

Notify.registerType('negative', {
  icon: 'error',
  color: 'negative',
  position: 'center'
})
