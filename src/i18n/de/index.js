function jsonSet (target, path, value) {
  let current = target
  path = [...path] // Detach
  const item = path.pop()
  path.forEach(function (key) {
    current[key] || (current[key] = {})
    current = current[key]
  })
  current[item] = value
  return target
}
function requireAll (r) {
  const gather = {}
  r.keys().forEach(function (mpath, ...args) {
    const result = r(mpath, ...args).default
    const path = mpath
      .replace(/(?:^[./]*\/|\.[^.]+$)/g, '') // Trim
      .split('/')
    jsonSet(gather, path, result)
  })
  if (!gather.index) delete gather.index
  return gather
}

const trans = requireAll(
  require.context(
    // Any kind of variables cannot be used here
    './', // (Webpack based) path
    true, // Use subdirectories
    /\.js$/ // File name pattern
  )
)

export default trans
