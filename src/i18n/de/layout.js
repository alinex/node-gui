export default {
  title: 'Alinex GUI',
  subtitle: 'Verwaltungsoberfläche',

  sidebarHide: 'Verstecken oder einblenden der linken Seitenleiste',

  nav: {
    cancel: 'Abbruch',
    close: 'Schließen',
    goBack: 'Zurück',
    set: 'Eintragen',
    add: 'Hinzufügen',
    update: 'Aktualisieren',
    exec: 'Ausführen',
    save: 'Speichern',
    delete: 'Löschen'
  },

  menu: {
    title: 'Hauptmenü',
    login: 'Anmelden',
    logout: 'Abmelden',
    fullscreenOn: 'Vollbildanzeige',
    fullscreenOff: 'Vollbildanzeige beenden',
    settings: 'Einstellungen',
    language: 'Sprachauswahl',
    about: 'Über GUI',
    dark: 'Nachtmodus'
  },

  home: {
    title: 'Start',
    subtitle: 'Modul Auswahl'
  },

  login: {
    title: 'Benutzeranmeldung',
    email: 'Email',
    emailError: 'Eine registrierte Email Adresse ist notwendig',
    password: 'Passwort',
    passwordError: 'Die Eingabe des kompletten Passworts ist notwendig',
    submit: 'Login',
    success: 'Anmeldung erfolgreich',
    fail: 'Anmeldung nicht möglich!'
  },

  logout: {
    success: 'Abmeldung Erfolgreich, Reset folgt...'
  },

  noConnection: 'Verbindungsproblem zum Server',
  notFound: 'Die angeforderte Seite wurde nicht gefunden...',
  failed: 'Aktion fehlgechlagen',
  success: 'Die Aktion wurde erfolgreich durchgeführt',
  saved: 'Erfolgreich gespeichert',
  noData: 'Keine Daten verfügbar.',
  disabled: 'Dieses Element ist derzeit deaktiviert.',
  search: 'Suchen'
}
