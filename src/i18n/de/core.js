export default {
  title: 'System Verwaltung',
  info: {
    title: 'Info',
    subtitle: 'System Informationen'
  },
  users: {
    title: 'Benutzer',
    subtitle: 'Konto Verwaltung',
    type: 'Benutzer',
    list: 'Keine Benutzer | Ein Benutzer | {count} Benutzer',
    detail: 'Detail',
    createButton: 'Neuer Benutzer',
    formLogin: 'Login Daten',
    formLoginDesc: 'Das aktuelle Passwort wird nicht angezeigt, gebe ein neues ein um es zu ändern.',
    loginType: {
      title: 'Login'
    },
    autologin: {
      title: 'IP/CIDR Liste für automatische Anmeldung'
    },
    email: {
      title: 'Email',
      error: 'Eine güötige Email Addresse wird zum Login benötigt'
    },
    password: {
      title: 'Passwort',
      titleNew: 'Neues Passwort',
      titleRepeat: 'Wiederholung Password',
      error: 'Das Passwort muss mindestend 6 Zeichen lang sein',
      errorMismatch: 'Das Passwort muss zweimal identisch angegeben werden'
    },
    formPersonal: 'Persönliche Daten',
    formPersonalDesc: 'Der Gravatar Dienst von WordPress wird benutzt um das Avatar Bild zu deiner Email Adresse abzurufen.',
    avatar: {
      title: 'Avatar',
      link: 'Anpassen des mit der Email Adresse verbundenen Avatars.'
    },
    nickname: {
      title: 'Pseudonym',
      error: 'Dein Pseudonym muss mindestens 4 Zeichen lang sein'
    },
    name: {
      title: 'Voller Name'
    },
    position: {
      title: 'Position',
      error: 'Deine Position sollte mindestens 3 Zeichen lang sein'
    },
    formAccess: 'Rollen',
    formAccessDesc: 'Berechtigungen, die der Nutzer über seine Rollen erhält.',
    disabled: {
      title: 'Gesperrt',
      desc: 'der Benutzer kann sich nicht mehr anmelden'
    },
    formAbilities: 'Rechte',
    formAbilitiesDesc: 'Detailierte Rechte, die durch die Rollen definiert sind (Aktualisiert nach Login).'
  },
  roles: {
    title: 'Rollen',
    type: 'Rolle',
    subtitle: 'Zugriffsrechte',
    list: 'Keine Rolle | Eine Rolle | {count} Rollen',
    detail: 'Detail',
    createButton: 'Neue Rolle',
    formMeta: 'Meta Daten',
    formMetaDesc: 'Beschreibungen zur Rolle.',
    name: {
      title: 'Name',
      error: 'Der Name wird zur Identifikation der Rolle benötigt.'
    },
    description: {
      title: 'Beschreibung'
    },
    disabled: {
      title: 'Gesperrt',
      desc: 'diese Rolle ist aktuell nicht nutzbar'
    },
    formRights: 'Zugriffsrechte',
    formRightsDesc: 'Die Regeln definieren die einzelnen Zugriffsrechte.',
    inverted: {
      title: 'Typ',
      desc: 'verbiete diesen Zugriff anstatt ihn zu erlauben'
    },
    action: {
      title: 'Aktion'
    },
    subject: {
      title: 'Bereich'
    },
    conditions: {
      title: 'Bedingung'
    },
    fields: {
      title: 'Felder'
    },
    formUsers: 'Benutzer',
    formUsersDesc: 'Anwender, denen diese Rolle zugewiesen ist.'
  }
}
