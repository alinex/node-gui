export default {
  title: 'Alinex GUI',
  subtitle: 'Control Interface for Server',

  sidebarHide: 'Hide or display left side menu',

  nav: {
    cancel: 'Cancel',
    close: 'Close',
    goBack: 'Go Back',
    set: 'Set',
    add: 'Add',
    update: 'Update',
    exec: 'Execute',
    save: 'Save',
    delete: 'Delete'
  },

  menu: {
    title: 'Main menu',
    login: 'Login',
    logout: 'Logout',
    fullscreenOn: 'Enable Fullscreen Mode',
    fullscreenOff: 'Disable Fullscreen Mode',
    about: 'About GUI',
    settings: 'Settings',
    language: 'Language',
    dark: 'Dark Mode'
  },

  home: {
    title: 'Home',
    subtitle: 'Dashboard'
  },

  login: {
    title: 'User Login',
    email: 'Email',
    emailError: 'We need your registered email',
    password: 'Password',
    passwordError: 'The password is mandatory',
    submit: 'Login',
    success: 'Successfully authenticated',
    fail: 'Could not login!'
  },

  logout: {
    success: 'Successfully logged out, reset will follow...'
  },

  noConnection: 'Could not connect to server',
  notFound: 'Ups, hier wurde nichts gefunden...',
  failed: 'Action failed',
  success: 'Action was successful',
  saved: 'Successfully stored',
  noData: 'No data available.',
  disabled: 'This element is currently disabled.',
  search: 'Search'

}
