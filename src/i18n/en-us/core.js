export default {
  title: 'System Administration',
  info: {
    title: 'Info',
    subtitle: 'System Information'
  },
  users: {
    title: 'Users',
    subtitle: 'Account Administration',
    type: 'user',
    list: 'No users | One user | {count} users',
    detail: 'Detail',
    createButton: 'Add new User',
    formLogin: 'Login Data',
    formLoginDesc: 'The current password will not be shown, set it if you want to change it.',
    loginType: {
      title: 'Login'
    },
    autologin: {
      title: 'IP/CIDR for automatic login'
    },
    email: {
      title: 'Email',
      error: 'A valid email address is needed to login'
    },
    password: {
      title: 'Password',
      titleNew: 'New Password',
      titleRepeat: 'Retype Password',
      error: 'The password should has at least 6 characters',
      errorMismatch: 'The password has to be two times exactly the same'
    },
    formPersonal: 'Personal Information',
    formPersonalDesc: 'The Gravatar service from WordPress is used to get the avatar icon linked to your email address.',
    avatar: {
      title: 'Avatar',
      link: 'Use WordPress to change Gravatar image linked with your email address.'
    },
    nickname: {
      title: 'Nickname',
      error: 'The nickname should have at least 4 characters'
    },
    name: {
      title: 'Full Name'
    },
    position: {
      title: 'Position',
      error: 'The position should have at least 3 characters'
    },
    formAccess: 'Roles',
    formAccessDesc: 'Abilities of the user which are given through roles.',
    disabled: {
      title: 'Disabled',
      desc: 'user will no longer be able to login'
    },
    formAbilities: 'Abilities',
    formAbilitiesDesc: 'Concret resolved abilities given by the above roles (updated after login).'
  },
  roles: {
    title: 'Roles',
    type: 'role',
    subtitle: 'Access Rights',
    list: 'No roles | One role | {count} roles',
    detail: 'Detail',
    createButton: 'Add new Role',
    formMeta: 'Meta Data',
    formMetaDesc: 'Descriptive information of role.',
    name: {
      title: 'Name',
      error: 'The name is needed as identifier to select the role.'
    },
    description: {
      title: 'Description'
    },
    disabled: {
      title: 'Disabled',
      desc: 'role will currently not be used'
    },
    formRights: 'Access Rights',
    formRightsDesc: 'The rules defining all abilities for this rule.',
    inverted: {
      title: 'Type',
      desc: 'disallow access instead of allow it'
    },
    action: {
      title: 'Action'
    },
    subject: {
      title: 'Subject'
    },
    conditions: {
      title: 'Conditions'
    },
    fields: {
      title: 'Fields'
    },
    formUsers: 'Users',
    formUsersDesc: 'People who have assigned this role.'
  }
}
