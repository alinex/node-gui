export default vue => {
  return [
    {
      title: vue.$t('core.info.title'),
      caption: vue.$t('core.info.subtitle'),
      icon: 'info',
      link: '/info'
    },
    {
      title: vue.$t('core.users.title'),
      caption: vue.$t('core.users.subtitle'),
      icon: 'mdi-account',
      link: '/users',
      service: 'users'
    },
    {
      title: vue.$t('core.roles.title'),
      caption: vue.$t('core.roles.subtitle'),
      icon: 'mdi-account-group',
      link: '/roles',
      service: 'roles'
    }
  ]
}
