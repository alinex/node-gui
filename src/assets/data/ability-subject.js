export default [
  { label: 'Authentication', value: 'authentication' },
  { label: 'Info', value: 'info' },
  { label: 'Checkup', value: 'checkup' },
  { label: 'Users', value: 'users' },
  { label: 'Roles', value: 'roles' }
]
