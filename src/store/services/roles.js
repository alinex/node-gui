import feathersClient, { makeServicePlugin, BaseModel } from '../../feathers'

class Roles extends BaseModel {
//  constructor (data, options) {
//    super(data, options)
//  }

    // Required for $FeathersVuex plugin to work after production transpile.
    static modelName = 'Role';
    // Define default properties here
    static instanceDefaults () {
      return {
        disabled: false
      }
    }
}

const servicePath = 'roles'
const servicePlugin = makeServicePlugin({
  Model: Roles,
  service: feathersClient.service(servicePath),
  servicePath
})

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
})

export default servicePlugin
