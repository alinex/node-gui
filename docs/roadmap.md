# Roadmap

**What's coming next?**

That's a big question on a freelancer project. The roadmap may change any time or progress may be stuck. But hopefully I will come further on in any way.

- don't upgrade to `@types/node@12` because of [electron problem](https://github.com/DefinitelyTyped/DefinitelyTyped/issues/10977#issuecomment-572350946)
- don't upgrade to socket.io-client v3.0.0 - connection not longer possible

## Version 0.5.0

-   login icon on dashboard
-   field ability checking
-   role view
    - abilities optimizer (on server update)
- strict password validation
- authentication management (lost-password, email verify)
- password generator

## Version 1.0.0

- google auth or github auth

{!docs/assets/abbreviations.txt!}
