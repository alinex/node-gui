title: Setup

# Setup

## Configuration

The configuration in `package.json` will define which browsers are supported using more or less polyfills. In example you may add old MS Internet Explorer 11 Support with:

```js
"browserslist": [
  "ie 11", // <<-- add it
  ...
]
```

## Builds

A development build is possible using:

    npm run dev

This enables you to change things and see the result in the browser in ashort time. But because the projects data has to be rejoined with the core each time it is not as fast as in single projects.
It automatically runs a local server which will reload and also refresh the browser if changes are made.

If everything looks fine you may build the different apps using `npm run build`, which will call the `bin/gui-setup`.

    bin/gui-setup

To specify the server to use which will be build into the client in the download files use the environment setting `API`:

    bin/gui-setup --api http://my-sever.de:3000

It will create distributable files within the `dist` folder which may be published to be used. But often you will call this from the [Alinex Server](https://alinex.gitlab.io/node-serever) or the project specific module extending it.

### Browser

To run the web versions a web server is needed, which can be simulated using:

    node_modules/.bin/quasar serve dist

or

    PORT=8000 node_modules/.bin/quasar serve dist

But first you have to build it like described above.

#### Single Page Application

A Single-Page Application (SPA) is a web application or web site that interacts with the user by dynamically rewriting the current page rather than loading entire new pages from a server. This approach avoids interruption of the user experience between successive pages, making the application behave more like a desktop application. But the initial loading will take some time.

In a SPA the appropriate resources are dynamically loaded and added to the page as necessary, usually in response to user actions. The page does not reload at any point in the process, nor does control transfer to another page, although the location hash or the HTML5 History API can be used to provide the perception and navigability of separate logical pages in the application.

This is build under dist folder.

#### Progressive Web Application

A Progressive Web App (PWA) is a web app that uses modern web capabilities to deliver an app-like experience to users.

As PWA the application will be mostly connectivity independent by using service workers to work offline or on low quality networks.

This alternative browser version is not used and build at the moment.

### Mobile Apps

Using [Capacitor](https://capacitor.ionicframework.com/) the web applications is transformed into a mobile app. It exposes native device APIs in the form of JavaScript modules.

If this is needed some [preparations](https://quasar.dev/quasar-cli/developing-capacitor-apps/preparation) has to be done.

Here you can build for two devices.

#### Android App

You need to install the Android SDK with cli tools and licenses agreed before `gui-setup` can successfully build it.

You will find the APK under `dist/download` folder.

#### IOS App

This is only possible to be made on a MacOS host with Xcode installed.

### Desktop Applications

To make also an desktop app the JavaScript is bundled into an [electron](https://electronjs.org/) app. Electron is the main GUI framework behind several notable open-source projects including GitHub’s Atom and Microsoft’s Visual Studio Code source code editors.

You will find the linux, darwin and win32 applications under `dist/download` folder.

### Dokumentation

The documentation (html+pdf+epub) will be build using:

    npm run pdf

But you can also always run a development server of the documentation ([mkdocs](https://alinex.gitlab.io/env/mkdocs/) needs to be installed):

    mkdocs serve

This will start an auto reloading documentation on <http://127.0.0.1:8000>

{!docs/assets/abbreviations.txt!}
