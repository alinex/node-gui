title: Overview

# Core Framework

As the Alinex GUI is no ready to use frontend, you have to use it as framework and build your real application with it. This section will describe the core GUI framework with it's components. To extend it best is to make a parent project like described under [development](../development).

!!! Attention

    This is not a complete manual, you have to know the basics of the [fundamental technologies](../technology) on which the GUI is build.

## Features

- [Internationalization](i18n.md)
- Dynamic loading using `import` in router

## 3rd Party Packages

Additional to the short descriptions here the original documentation may be useful:

- [Vue](https://vuejs.org/v2/guide/)
  - [VueX](https://vuex.vuejs.org/guide/) for application state
  - [VueX Feathers](https://vuex.feathersjs.com/api-overview.html#features)
- [Quasar Framework](https://quasar.dev/)
  - [Style](https://quasar.dev/style/typography)
- Icons: [Material](https://material.io/icons/) or [MDI](https://materialdesignicons.com/) using `mdi-xxx`

{!docs/assets/abbreviations.txt!}
