title: Components

# Special Components

The following special Alinex components are contained within the core.

## Global

### Notify

To give some response messages to the user the quasar notify object is used. This can be done within a vue file:

```js
// log the error to the browser console using log(), warn() or error()
console.log(message)
// show the visible element
this.$q.notify({
    type: 'positive',
    message
})
```

Or outside of a vue file (as negative message):

```js
// load the plugin
import { Notify } from 'quasar'

// log the error to the browser console using log(), warn() or error()
console.warn(message)
// show the visible element
Notify.create({
    type: 'negative', // or positive
    message: this.$t('layout.failed'),
    caption: message // caption will add a second line for a sub message
})
```

The plugin is predefined to display positive messages on the bottom of the page, negative ones directly in the center of the screen. An additional logging to console should always be made. For debugging this makes it possible to see what was done last.

## Dialogs

-   **Dialog** (ax-dialog)

    This is the base setup to make all the dialogs the same. It contains the card layout with a titlebar with `title` and `icon` properties. The content can be set with the default slot and the `model` property is used for the visibility state.

-   **About Dialog** (ax-about)

    An implementation of a dialog with the about section containing information from package.json and the policy from the translation properties under group `about.*`.

-   **Login Dialog** (ax-login)

    An implementation of a dialog with the login form for local authentication complete with the login procedure. This maybe extended later to also support missing password or other authentication types.

!!! Example

    ```vue
    <template>
        <ax-dialog :model.sync="localModel" :title="xxxxxxx" icon="info">
            <q-card-section class="q-pt-none">
                <p>Here comes the content...</p>
            </q-card-actions>
            <!-- more card sections are possible -->
        </ax-dialog>
    </template>

    <script>
    import axDialog from 'components/dialog'

    export default {
      name: 'MyDialog',

      props: {
        model: { type: Boolean, required: true }
      },

      components: { axDialog },

      data () {
        // locally used data
      },

      computed: {
        // used to keep model in sync
        localModel: {
          get () { return this.model },
          set (localModel) { this.$emit('update:model', localModel) }
        }
      }
    }
    </script>
    ```

## Module Organization

The following components are used to include modules into the layout in sidebar and as dashboard icon.

-   **Sidebar Extend** (ax-sidebar-extend)

    Placeholder used to be overwritten in parent project to add more menu entries using the following link component.

-   **Sidebar Link** (ax-sidebar-link)

    Entry in the sidebar configured using properties: `title`, `caption`, `icon`, `link`, `service`.

-   **Index Extend** (ax-index-extend)

    Placeholder used to be overwritten in parent project to add modules to dashboard using the following icon component.
    This works identical to the sidebar extension.

-   **Index Icon** (ax-index-icon)

    Entry in the sidebar configured using properties: `title`, `caption`, `icon`, `link`, `service`.

!!! Example

    ```vue
    <template>
      <div class="icons row">
        <ax-index-icon
            v-for="link in links"
            :key="link.title"
            v-bind="link" />
      </div>
    </template>

    <script>
    import axIndexIcon from 'components/index-icon'

    const list = vue => {
      return [
        {
          title: vue.$t('mymodule.fn1.title'),
          caption: vue.$t('mymodule.fn1.subtitle'),
          icon: 'mdi-movie-search',
          link: '/mymodule/fn1'
        },
        {
          title: vue.$t('mymodule.fn2.title'),
          caption: vue.$t('mymodule.fn2.subtitle'),
          icon: 'mdi-table-large',
          link: '/mymodule/fn2'
        }
      ]
    }

    export default {
      name: 'indexExtend',

      components: { axIndexIcon },

      computed: {
        links () {
          return list(this)
        }
      }
    }
    </script>
    ```

{!docs/assets/abbreviations.txt!}
