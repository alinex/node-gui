title: CRUD Object

# Object Create, Read, Update and Delete

This shows on the example of the user object how to administrate it in the GUI.

To do this you need:

- routing
- store
- pages
- translations

See below how to do it.

## Routing

## Store

Feathers-vuex focuses on abstracting away the Vuex syntax in favor of using Model classes

## Pages

## Security

The authorization works based on [abilities](https://alinex.gitlab.io/node-server/usage/access). They will be checked in multiple places if the abilities which a user has through his roles matches the abilities needed to access:

1. Within the router the `meta.ability` setting contains the right needed to access this route (`[action, subject, field]`).
2. In the page itself using the `v-if="$can('read', service)` attribute

This will be checked through the `boot/casl.js` plugin in the router loading of abilities will be done with the login.

## Translations

{!docs/assets/abbreviations.txt!}
