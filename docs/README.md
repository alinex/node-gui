# Alinex GUI Client

![portal icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/17570134/gui-icon.png){: .right .icon}

This is the client used for the **Alinex Server**. It's an interface to do access and work with the server through a graphical interface. It should be a base for specialized tools for specific environments which can be customized to it's needs.

It is included in the [Alinex Server](https://gitlab.com/alinex/node-server).

![Home](usage/pc-home.png){: style="width:45%"}
![Login](usage/pc-login.png){: style="width:45%"}
![User List](usage/pc-users.png){: style="width:45%"}
![User Edit](usage/pc-users-details.png){: style="width:45%"}
![Dark Mode](usage/pc-dark.png){: style="width:45%"}
![Mobile](usage/mobile-home.png){: style="height:200px"}
![Mobile](usage/mobile-sidebar.png){: style="height:200px"}

See more screenshots under [usage](usage/README.md).

Big Features:

- modern design, responsive
- different web technologies (SPA, PWA, SSR)
- mobile apps (Android, IOS)
- desktop application (Windows, Mac, Linux)
- multi lingual with dark mode

currently about 85% of current browser share is supported by default:

- Chrome for Android >= 81
- Firefox for Android >= 68
- Android >= 81
- Chrome >= 73
- Edge >= 79
- Firefox >= 68
- iOS >= 10.0-10.2
- Opera >= 64
- Safari >= 10.1

## Chapters

Read all about the [alinex-gui](https://alinex.gitlab.io/node-gui) in the chapters (top navigation menu):

- [Usage](usage) - user manual on how to work with examples of screens and explanations
- [Framework](framework) - manual on how to extend functionality
- [Developer](core) - detailed developer information

If you want to have an offline access to the documentation, feel free to download: [alinex-gui.pdf](alinex-gui.pdf) or [alinex-gui.epub](alinex-gui.epub).

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-gui/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use for all of them the [GitLab Issues](https://gitlab.com/alinex/node-gui/-/issues) which only requires you to register with a free account.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
