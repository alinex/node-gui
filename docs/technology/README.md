title: Overview

# Used Technologies

We also don't reinvent the wheel, so we base this framework on some other libraries which we saw as most fitting at the moment we started this project. It was no selection out of space but with some tryouts before.

The biggest technologies are:

-  [VueJS](vue.md) - A progressive framework for building user interfaces for the web.
-  [Quasar Framework](quasar.md) - Composition of VueJS components with a framework to build different apps.
-  [State Management](vuex.md) - Vuex Store is used as state management holding the data.
-  [Feathers](feathers.md) - Integration of the Feathers Client into VueJS with state management.

All this is based on [JavaScript](https://alinex.gitlab.io/js/) as client language and for the build tool.

Other, smaller modules are also included which are not of such big concern.

{!docs/assets/abbreviations.txt!}
