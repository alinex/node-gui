# Vue.js

Vue.js is a web framework for building the view component of web applications.

## Basics

Vue.js uses vue files as a base which are a combination of HTML + JavaScript + Style like:

```vue
<template>
    <div id="app">
    {{ message }}
    </div>
</template>

<script>
export default {
    name: 'App',
    data: {
        message: 'Hello Vue!'
    }
}
</script>

<style>
/* possible css styles */
</style>
```

Templates can use:

- Binding: Use `<a v-bind:href="url">` or `<a :href="url">` to bind its value to the given JavaScript expression or variable.
- Dynamic Attributes: `<a v-bind:[attrName]='url'>` or `<a :[attrName]='url'>` will set the attribute defined within attrName but no expressions, as attribute names (use computed properties therefore).
- Conditionals: `<span v-if="flag">Flag is true</span>` will show only if flag variable is true.
  Also the use of `v-else` or `v-else-if` in the direct following element is possible.
- Keys: The `key` attribute within elements tells vue to trade it as individuals.
- Show: Using `v-show` will switch visibiliy, while it is always rendered.
- Loops: `<li v-for="todo in todos">{{ todo.text }}</li>` will add the li tag multiple times.
- User Input: `<button v-on:click="fn">` or `<button @click="fn">` will call function if clicked.
- Modifiers: `<form v-on:submit.prevent="onSubmit">` will call event.preventDeault()
- 2-Way-Binding: `<input v-model.trim="msg">

### JavaScript

To put some of the complexity out of the template itself and make this simpler, you can add it to the script.

- `data` is a map with values to be directly used.
- `computed` contains an alternative map with functions which will be called at the time of use.
- `methods` are a collection of functions to be called.
- `computed` defines `set`ter and `get`ter methods per property which will be called if the property is read or changed.
- `watch` defines functions per properties, which are called if the property has changed.

Computed properties will only update if their used reactive element changes.

### Components

The components let a big application be build from smaller and reusable pieces which will be put together hierarchically. To make a complex application more manageable it should be split into multiple, small units.

<https://medium.com/quasar-framework/component-building-with-quasar-fc101b6730ae>

{!docs/assets/abbreviations.txt!}
