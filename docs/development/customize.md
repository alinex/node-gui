title: Customize

# Customization

## Logo

This can be changed by overwriting the default with an image under the following position:
- `src/assets/logo.png`
- `src/assets/logo-black.png`

The `-black` variant is used in dark mode. A transparent picture therefore may be identical.

## Colors

The file `src/css/quasar.variables.scss` contains the colors used for the default names. To change the theme to your colors simple copy this file from the Alinex GUI and change the color codes. You may use the [Quasar Theme Builder](https://quasar.dev/style/theme-builder) to select and see the most colors together.

{!docs/assets/abbreviations.txt!}
