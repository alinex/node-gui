title: Project

# Parent Project

The following pages will explain how your specific own system is build with the Alinex GUI as a base.

## Setup

To make your own project you need the following files:

=== "package.json"

    ```json
    {
      "name": "my-gui",
      "title": "My GUI",
      "version": "0.1.0",
      "build": "SNAPSHOT",
      "description": "Human interface to my server with it's applications.",
      "productName": "My-GUI",
      "cordovaId": "de.alinex.my-gui",
      "capacitorId": "",
      "responsible": "Alexander Schilling",
      "foreignData": "",
      "private": true,
      "author": "Alexander Schilling",
      "scripts": {
        "dev": "bin/gui-dev",
        "lint": "eslint --ext .js,.vue src --fix --ignore-path .gitignore ./",
        "test": "echo \"No test specified\" && exit 0",
        "play": "quasar dev -m cordova -T android",
        "build": "rm -r dist/* && npm run build-spa && npm run build-pwa && npm run build-linux && npm run build-darwin && npm run build-win32 && npm run build-android",
        "build-spa": "quasar build && tar -C dist -czf dist/Alinex-GUI_online-spa.tgz spa",
        "build-pwa": "quasar build -m pwa && tar -C dist -czf dist/Alinex-GUI_online-pwa.tgz pwa",
        "build-ssr": "quasar build -m ssr",
        "build-linux": "quasar build -m electron -T linux && tar -C dist/electron -czf dist/Alinex-GUI_linux-x64.tgz Alinex-GUI-linux-x64 >/dev/null",
        "build-darwin": "quasar build -m electron -T darwin && tar -C dist/electron -czf dist/Alinex-GUI_darwin-x64.tgz Alinex-GUI-darwin-x64 >/dev/null",
        "build-win32": "quasar build -m electron -T win32 && cd dist/electron && zip -r ../Alinex-GUI_win32-x64.zip Alinex-GUI-win32-x64 >/dev/null && cd ../..",
        "build-android": "ANDROID_HOME=~/Android/Sdk quasar build -m capacitor -T android && cp dist/capacitor/android/apk/release/app-release-unsigned.apk dist/Alinex-GUI_android.apk",
        "build-ios": "test \"$OSTYPE\" = \"darwin\" && ( quasar build -m capacitor -T ios ) || echo 'ios build is only possible on MacOS with Xcode installed.'",
        "pdf": "./mkdocs-pdf.sh",
        "preversion": "npm test && npm run pdf && git add -A && git commit -m \"Update PDF documentation\"",
        "postpublish": "git push origin --all && git push origin --tags"
      },
      "dependencies": {
        "@alinex/gui": "^0.1.0",
        "@feathersjs/authentication-client": "^4.5.2",
        "@feathersjs/feathers": "^4.5.2",
        "@feathersjs/rest-client": "^4.5.2",
        "@feathersjs/socketio-client": "^4.5.2",
        "@quasar/extras": "^1.0.0",
        "@vue/composition-api": "^0.5.0",
        "axios": "^0.18.1",
        "feathers-hooks-common": "^5.0.2",
        "feathers-vuex": "^3.9.1",
        "flag-icon-css": "^3.4.6",
        "quasar": "^1.0.0",
        "socket.io-client": "^2.3.0",
        "vue-i18n": "^8.0.0",
        "vuelidate": "^0.7.5"
      },
      "devDependencies": {
        "@capacitor/ios": "^1.5.2",
        "@quasar/app": "^1.0.0",
        "@quasar/quasar-app-extension-icon-genie": "^1.1.3",
        "babel-eslint": "^10.0.1",
        "devtron": "^1.4.0",
        "electron": "^8.2.0",
        "electron-debug": "^3.0.1",
        "electron-devtools-installer": "^2.2.4",
        "electron-packager": "^14.2.1",
        "eslint": "^6.8.0",
        "eslint-config-airbnb-base": "^14.0.0",
        "eslint-config-standard": "^14.1.1",
        "eslint-loader": "^3.0.3",
        "eslint-plugin-import": "^2.20.1",
        "eslint-plugin-node": "^11.0.0",
        "eslint-plugin-promise": "^4.2.1",
        "eslint-plugin-standard": "^4.0.1",
        "eslint-plugin-vue": "^6.1.2"
      },
      "engines": {
        "node": ">= 10.18.1",
        "npm": ">= 6.13.4",
        "yarn": ">= 1.21.1"
      },
      "browserslist": [
        "last 10 Chrome versions",
        "last 10 Firefox versions",
        "last 4 Edge versions",
        "last 7 Safari versions",
        "last 8 Android versions",
        "last 8 ChromeAndroid versions",
        "last 8 FirefoxAndroid versions",
        "last 10 iOS versions",
        "last 5 Opera versions"
      ],
      "resolutions": {
        "@babel/parser": "7.7.5"
      }
    }
    ```

=== ".eslint.rc"

    ```js
    module.exports = {
      root: true,

      parserOptions: {
        parser: "babel-eslint",
        sourceType: "module"
      },

      env: {
        browser: true
      },

      extends: [
        "standard",
        //'airbnb-base',
        // Uncomment any of the lines below to choose desired strictness,
        // but leave only one uncommented!
        // See https://eslint.vuejs.org/rules/#available-rules
        "plugin:vue/essential" // Priority A: Essential (Error Prevention)
        // 'plugin:vue/strongly-recommended' // Priority B: Strongly Recommended (Improving Readability)
        // 'plugin:vue/recommended' // Priority C: Recommended (Minimizing Arbitrary Choices and Cognitive Overhead)
      ],

      // required to lint *.vue files
      plugins: ["vue"],

      globals: {
        ga: true, // Google Analytics
        cordova: true,
        __statics: true,
        process: true,
        Capacitor: true,
        chrome: true
      },

      // add your custom rules here
      rules: {
        "no-param-reassign": "off",

        "import/first": "off",
        "import/named": "error",
        "import/namespace": "error",
        "import/default": "error",
        "import/export": "error",
        "import/extensions": "off",
        "import/no-unresolved": "off",
        "import/no-extraneous-dependencies": "off",
        "import/prefer-default-export": "off",
        "prefer-promise-reject-errors": "off",

        // allow debugger during development only
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
      }
    };
    ```

Now you can setup the application:

```bash
mkdir bin
cd bin
ln -s ../node_modules/@alinex/gui/bin/gui-* .
cd ..
npm install
```

Now you can use `npm run dev` or `npm run build`.

{!docs/assets/abbreviations.txt!}
