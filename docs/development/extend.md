title: Extending

# Extending Modules

## Routing

Within the application the routing will decide which page to show. The core routes can be extended using:

=== "src/router/extend.js"

    ```js
    export default [
      {
        path: '/myapp',
        component: () => import('layouts/Default.vue'),
        children: [
          { path: '', component: () => import('pages/myapp/index') },
          {
            path: 'info',
            component: () => import('pages/myapp/info'),
            meta: { module: 'core.info' }
          }
        ]
      }
    ]
    ```

## Pages

The title will be set by the layout but can also be set here to something other:

```js
meta () {
    return { title: this.$t(`${this.$route.meta.module}.title`) }
}
```

## Menu

To add new modules they need to be appended to the sidebar and to the start page. Therefore the following changes have to be made.
We add two menus, both using separate files to keep it more clean.

=== "src/assets/data/my-menu.js"

    ```js
    export default vue => {
      return [
        {
          title: vue.$t('core.info.title'),
          caption: vue.$t('core.info.subtitle'),
          icon: 'info',
          link: '/info'
        },
        {
          title: vue.$t('core.users.title'),
          caption: vue.$t('core.users.subtitle'),
          icon: 'mdi-account',
          link: '/users',
          service: 'users'
        },
        {
          title: vue.$t('core.roles.title'),
          caption: vue.$t('core.roles.subtitle'),
          icon: 'verified_user',
          link: '/roles',
          service: 'roles'
        }
      ]
    }
    ```

=== "src/components/sidebar-extend.vue"

    ```vue
    <template>
        <ax-sidebar-menu-one />
        <ax-sidebar-menu-two />
    </template>

    <script>
    import axSidebarMenuOne from 'components/sidebar-mymenu-one'
    import axSidebarMenuTwo from 'components/sidebar-mymenu-two'

    export default {
      name: 'sidebarExtend',

      components: { axSidebarMenuOne, axSidebarMenuTwo }
    }
    </script>
    ```

=== "src/components/sidebar-menu1.vue"

    ```vue
    <template>
        <q-expansion-item switch-toggle-side expand-separator :label="$t('mymenu.title')">
            <ax-sidebar-link
                v-for="link in links"
                :key="link.title"
                v-bind="link"
            />
        </q-expansion-item>
    </template>

    <script>
    import axSidebarLink from 'components/sidebar-link'
    import myMenu from 'assets/data/my-menu'

    export default {
      name: 'sidebarMenu1',

      components: { axSidebarLink },

      computed: {
        links () {
          return myMenu(this)
        }
      }
    }
    </script>
    ```

## Dashboard

And at last the additional pages can also be added to the dashboard as icons using:

=== "src/components/index-extend.vue"

    ```vue
    <template>
      <div class="icons row">
        <ax-index-icon
            v-for="link in links"
            :key="link.title"
            v-bind="link"
        />
      </div>
    </template>

    <script>
    import axIndexIcon from 'components/index-icon'
    import myMenu from 'assets/data/my-menu'

    export default {
      name: 'indexExtend',

      components: { axIndexIcon },

      computed: {
        links () {
          return myMenu(this)
        }
      }
    }
    </script>
    ```

{!docs/assets/abbreviations.txt!}
