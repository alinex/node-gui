title: Access Control

# Management of Access Control Settings

As admin you can setup the user rights.

![User List](pc-users.png){: style="width:95%"}
![User Details](pc-users-details.png){: style="width:95%"}

This allows to create and update user records and extend their rights by setting their roles.

To extend the roles with new ones:

![Role List](pc-users.png){: style="width:95%"}
![Role Details](pc-users-details.png){: style="width:95%"}

You should know the REST interface and data structure to define this. Maybe the swagger is enabled on the server which can help giving structure information.

{!docs/assets/abbreviations.txt!}
