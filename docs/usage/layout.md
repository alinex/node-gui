title: Layout

# Basic Layout

At first the system will greet you with a simple dashboard, only containing the few elements available without authentication.

![Home](pc-home.png){: style="width:95%"}

The layout is responsive and also is also possible on mobile phones or tablets.

![Mobile](mobile-home.png){: style="width:40%"}
![Mobile Sidebar](mobile-sidebar.png){: style="width:40%; margin-left:60px"}

To see more a login is necessary. Which will give the user more rights.

![Login](pc-login.png){: style="width:95%"}

Now the dashboard on the home page shows some more links.

![Dashboard](pc-dashboard.png){: style="width:95%"}

A dark mode is also possible, which will be set automatically if detectable.

![Dark](pc-dark.png){: style="width:95%"}

{!docs/assets/abbreviations.txt!}
