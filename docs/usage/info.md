title: Info

# System Information

The system information is needed to help giving the developer more details about the current setup.

![Login](pc-info.png){: style="width:95%"}

The following information is included:

- client with container software and application
- user with roles and abilities
- server with system user and nodejs version
- host hardware and operating system
- store databases

{!docs/assets/abbreviations.txt!}
