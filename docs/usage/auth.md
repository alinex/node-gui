# Authentication

In the moment only a simple email/password authentication is possible. No cookies are needed here.

![Login](pc-login.png){: style="width:95%"}

On logout the client will be reseted, after some seconds.

{!docs/assets/abbreviations.txt!}
