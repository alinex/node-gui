title: Overview

# User Manual

This user manual only describes the Alinex core functionality. Because Alinex is a framework your individual application will have a lot more, which is not included here.

The core contains:

- Layout
- Authentication
- Access Control Management
- System Information

{!docs/assets/abbreviations.txt!}
