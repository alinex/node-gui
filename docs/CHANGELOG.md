# Last Changes

-   update documentation theme

## Version 0.4.2 (08.11.2020)

-   fix socketio timeout to 5 Minutes
-   fix changing password / no password
-   update packages and doc structure
-   add autologin field to user
-   replace checks with checkup

## Version 0.4.1 (14.10.2020)

-   fix include flags in build

## Version 0.4.0 (12.10.2020)

-   role administration
-   sidebar open on access to subentry
-   fix ability check to also support conditions
-   roles edit in user view
-   abilities display for own user
-   extracted subject possibilities to data module
-   abilities in info dialog optimized
-   show users of role and role abilities in user
-   show disabled state visually
-   readonly mode if field is not updateable

## Version 0.3.0 (31.07.2020)

-   restructured translation index
-   updated framework docs
-   support additional project routes
-   updated base framework and dropped ie11 support (per default)
-   faster development mode in parent project
-   check abilities in router
-   login placeholder only in demo
-   start page in sidebar not always highlighted
-   reset on logout for data cleanup
-   create/update/delete user now working

## Version 0.2.0 (14.06.2020)

-   automated build
-   project support
-   authentication
-   logout
-   store state in session
-   check abilities in router
-   module layout with sidebar
-   info service
-   privacy like about dialog
-   check abilities for menu and icon view

## Version 0.1.0 (05.05.2020)

-   initial setup
-   i18n
    -   deep structure
    -   language selection
-   dialog components
-   modularization
-   login form
-   building all aplications

{!docs/assets/abbreviations.txt!}
