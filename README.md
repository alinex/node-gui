# Alinex GUI Client

This is the client used for the **Alinex Server**. It's an interface to do access and work with the server through a graphical interface. It should be a base for specialized tools for specific environments which can be customized to it's needs.

It is included in the [Alinex Server](https://gitlab.com/alinex/node-server).

## Documentation

Find a complete manual under [alinex.gitlab.io/node-gui](https://alinex.gitlab.io/node-gui).

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/node-gui/alinex-gui.pdf).

## License

(C) Copyright 2020 - 0221 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
